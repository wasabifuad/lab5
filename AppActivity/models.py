from django.db import models
from django.conf import settings

# Create your models here.
class Activity(models.Model):
    name = models.CharField(max_length = 20)
    category = models.CharField(max_length = 20)
    place = models.CharField(max_length = 20)
    date = models.DateField()
    time = models.TimeField()
    
    def __str__(self):
        return self.name

# class Activities(models.Model):
#     name = models.CharField(max_length = 20)
#     category = models.CharField(max_length = 20)
#     place = models.CharField(max_length = 20)
#     date = models.DateField()
#     time = models.TimeField()
    
#     def __str__(self):
#         return self.name