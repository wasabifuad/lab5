from django import forms
from .models import Activity 
class activity_form(forms.ModelForm):
    
    class Meta:
        model = Activity
        fields = ['name', 'category', 'place', 'date', 'time' ]
        widget = {
            'date' : forms.DateInput(attrs = {'type':'date'}),
            'time' : forms.TimeInput(attrs = {'type':'time'})
        }