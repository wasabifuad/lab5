from django.shortcuts import render
from django.http import HttpResponse
# from .forms import ContactForm
# from .forms import ScheduleForm

# Create your views here.
def landingPage (request):
    return render (request, 'landing-page.html')
def profilePage (request):
    return render (request, 'profile-page.html')
def tuturTegur (request):
    return render (request, 'tutur-tegur.html')
# def activityPage (request):
#     if(request.method == 'POST'):
#         form = ScheduleForm(request.POST)
#         if form.is_valid():
#             post = form.save()
#             post.save()
#         return redirect
#     return render (request, 'activity-page.html')

